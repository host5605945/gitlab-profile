# Hosting Gitlab Space 1

> Group for deploying static web apps directly accessible to anyone with an internet connection.


## Hosted Lists
- Jupyterlite : https://host5605945.gitlab.io/jupyterlite/lab/index.html
- Keep Content : https://keep-content-host-private-c4d02c58dc7bf6c2bada1b031657d66a18945.gitlab.io/
- SvelteKB : https://sveltekb-host5605945-e1c9fc3bc53c53f0cdfd2bf8e4e6fbba1016bfbfde.gitlab.io/
- PdfJs viewer : https://pdfjs-modern-host5605945-26ab886ba1ecc176e6949cab68d0c73293dc26.gitlab.io/web/viewer.html?file=
- DWM Static version v.1.2.0 : https://tranglyslivermerketimesmuncrefam-host5605945-967463913af533c99f.gitlab.io/

